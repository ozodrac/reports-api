FROM php:7.2.6-apache
RUN docker-php-ext-install mysqli pdo pdo_mysql
RUN a2enmod rewrite

# Install Composer
RUN apt-get -y update && \
    apt-get -y install curl nano git vim zip unzip unzip libxml2-dev && \
    curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
    
# Install Postgre PDO
RUN apt-get install -y libpq-dev \
    && docker-php-ext-configure pgsql -with-pgsql=/usr/local/pgsql \
    && docker-php-ext-install pdo pdo_pgsql pgsql

# Configuration for Apache
COPY apache/000-default.conf /etc/apache2/sites-available

# Clean after install
RUN apt-get autoremove -y && apt-get clean all


RUN chown -R www-data:www-data /var/www/html
ADD ./src /var/www/html

# Change working directory
WORKDIR /var/www/html


# Create Laravel folders (mandatory)
RUN mkdir -p /var/www/html/bootstrap/cache
RUN mkdir -p /var/www/html/bootstrap/cache

RUN mkdir -p /var/www/html/storage/framework
RUN mkdir -p /var/www/html/storage/framework/sessions
RUN mkdir -p /var/www/html/storage/framework/views
RUN mkdir -p /var/www/html/storage/meta
RUN mkdir -p /var/www/html/storage/cache
RUN mkdir -p /var/www/html/public/storage/uploads/

# Change folder permission
RUN chmod -R 0777 /var/www/html/storage/
RUN chmod -R 0777 /var/www/html/storage/framework
RUN chmod -R 0777 /var/www/html/storage/framework/sessions
RUN chmod -R 0777 /var/www/html/storage/framework/views
RUN chmod -R 0777 /var/www/html/public/storage/uploads/
# Laravel writing rights
RUN chgrp -R www-data /var/www/html/storage /var/www/html/bootstrap/cache
RUN chmod -R ug+rwx /var/www/html/storage /var/www/html/bootstrap/cache